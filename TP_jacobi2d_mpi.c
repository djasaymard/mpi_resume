#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "timer.h"
#include <mpi.h>


#define NN 4
#define NM 4
#define MAX(x, y) (((x) >(y)) ? (x) : (y))

/*double A[NN][NM];
double Anew[NN][NM];*/

void writeSolution(double);
int checkSolution(double);

int main(int argc, char** argv)
{
    int id, p;
     MPI_Status status;
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    //double A[NN][NM];
    //double Anew[NN][NM];
    int i,j;
    const int n = NN;
    const int m = NM;
    const int iter_max = 60;
    
    const double tol = 1.0e-6;
    float error     = 1.0;
    float er=1.0;
    //
     float *A;
     float *Anew ;
     float *Ar=(float*)malloc(sizeof(float*) * (n*m)/p);
     float *Anewr= (float*)malloc(sizeof(float*)* (n*m)/p);   
     //Anew = (double*)malloc(sizeof(double*) *  n*m);   
     

     if(id==0)
     {
        printf("Jacobi relaxation Calculation: %d x %d mesh\n", n, m);
       
           A=(float*)malloc(sizeof(float*) * n*m);
           Anew=(float*)malloc(sizeof(float*) * n*m);  
           
         for(j=0; j<n; j++){
            for(i=0; i<m; i++){
                 /*A[j][i]    = 0.0;
                 Anew[j][i] = 0.0;*/
                    A[j*n + i]= 0.0;
                    Anew[j* n +i]= 0.0;
            }
          }  
                        
         for ( j = 0; j < n; j++)
         {   // A[j][0]    = 1.0;
             //Anew[j][0] = 1.0;
             A[j*n + 0]    = 1.0;
             Anew[j*n +0]    = 1.0;
         } 
     } // end if id=0     
    
      MPI_Scatter(A,(n*m)/p, MPI_FLOAT,Ar, (n*m)/p,MPI_FLOAT, 0, MPI_COMM_WORLD);
      MPI_Scatter(Anew,(n*m)/p, MPI_FLOAT,Anewr, (n*m)/p,MPI_FLOAT, 0, MPI_COMM_WORLD);
      
     /* if(id!=0)
      {
       printf(" sur processus %d:\n", id);
        int k;
        for(k=0; k<(n*m)/p; k++)
        {           
           printf(" %0.2f", Ar[k]);
         }    
      }

      if(id==0)
      {
        printf(" sur 0:\n");
        int k,l;
        for(k=0; k<n; k++)
        {  
          for(l=0; l<m; l++){
           printf(" %0.2f", A[k*n + l]);}
        }

      }*/
      
   
  //printf("Jacobi relaxation Calculation: %d x %d mesh\n", n, m);
    
   StartTimer();    
   int iter = 0;
   
   float x,u,v;
   float y;
   float c,d;
   
while ( er > tol && iter < iter_max )
{  
  int h;   
    
        error = 0.0;   
        er=0.0;   
        for( i = 1; i < ((n*m)/p -1); i++)
        {                        
             if(id==0)
             {
               u=Ar[i];
               MPI_Send(&u,1, MPI_FLOAT,id+1,0,MPI_COMM_WORLD);
             }

             if(id==  (n*m/p)-1 ) //3
             {   
               v=Ar[i];
               MPI_Send(&v,1, MPI_FLOAT,id-1,0,MPI_COMM_WORLD);
             }

             if(id!=0 && id!=(n*m/p)-1 ) //diff de 0 et 3
             {
               u=Ar[i];
               v=Ar[i]; 

              if (id!=(n*m/p)-1 -1)/*(id!=2)*/MPI_Send(&u,1, MPI_FLOAT,id+1,0,MPI_COMM_WORLD);
              if (id!=0+1) MPI_Send(&v,1, MPI_FLOAT,id-1,0,MPI_COMM_WORLD);              

              MPI_Recv(&u,1,MPI_FLOAT, id-1,0,MPI_COMM_WORLD, &status);  //2e zero est juste un tag
              MPI_Recv(&v,1,MPI_FLOAT, id+1,0,MPI_COMM_WORLD, &status);
           
              Anewr[i]= 0.25 * ( Ar[i+1]+ Ar[i-1] + u + v);
                                       
              //error = MAX(error, fabs(Anew[j][i] - A[j][i])); 
              er = MAX(0, fabs(Anewr[i] - Ar[i])) ; 
             /*if(id==2)*/MPI_Send(&er,1, MPI_FLOAT,0,0,MPI_COMM_WORLD);
                  
              //MPI_Reduce(&er,&er,1, MPI_FLOAT, MPI_MAX,0, MPI_COMM_WORLD);

              // printf(" %0.6f\n", error);
              //if(id==1)/*(iter % 100 == 0)*/printf("\n proc: %d %5d, %0.6f\n",id, iter, error);
     }         
  }  
     
     for( j = 1; j < (n*m/p) -1; j++)
      {            
        Ar[j] = Anewr[j];         
      }

     
      /*if(iter % 100 == 0)printf("\n%5d, %0.6f\n", iter, error);*/
        if(id==0)
          {   MPI_Recv(&er,1,MPI_FLOAT, MPI_ANY_SOURCE,0,MPI_COMM_WORLD, &status);
              printf(" %5d, %0.6f\n", iter,er);
             //printf("\n%5d, %0.6f\n", iter, error);}
          }
      //}     
       
      iter++;         
      er ++;  
      //er++;       
 }

 //on reassemble

 MPI_Gather(Anewr,(n*m)/p, MPI_FLOAT,Anew,(n*m)/p,MPI_FLOAT,0, MPI_COMM_WORLD);
 MPI_Gather(Ar,(n*m)/p, MPI_FLOAT,A,(n*m)/p,MPI_FLOAT,0, MPI_COMM_WORLD);

   if(id==0){
      for( j = 1; j < n-1; j++)
        { 
            for( i = 1; i < m-1; i++ )
            {
                 A[j*n + i] = Anew[j*n +i];    
            }
      }
      /* int k,l;   //afficher pr verif
        for(k=0; k<n; k++)
        {  
          for(l=0; l<m; l++){
           printf(" %0.2f\n", A[k*n + l]);}
        }*/
  }
 
   double runtime = GetTimer();     

    /*writeSolution(error); 
    int passed = checkSolution(error);
    if (passed == 0) {
  printf("Error check passed.\n");
    } else {
  printf("Error check failed.\n");
    }*/
  
  
   //printf(" total: %f s\n", runtime / 1000);   

printf("end");
   MPI_Finalize();
   return EXIT_SUCCESS;  
}



