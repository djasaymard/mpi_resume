#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "timer.h"


#define NN 4
#define NM 4
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

/*double A[NN][NM];
double Anew[NN][NM];*/

void writeSolution(double);
int checkSolution(double);

int main(int argc, char** argv)
{
    double A[NN][NM];
    double Anew[NN][NM];
    int i,j;
    const int n = NN;
    const int m = NM;
    const int iter_max = 60;
    
    const double tol = 1.0e-6;
    double error     = 1.0;
   
     
         for ( j = 0; j < n; j++)
          {
            for ( i = 0; i < m; i++)
            {
                 A[j][i]    = 0.0;
                 Anew[j][i] = 0.0;
            }
           }    
            
      for ( j = 0; j < n; j++)
        {
             A[j][0]    = 1.0;
             Anew[j][0] = 1.0;
        }
       
    
   
  printf("Jacobi relaxation Calculation: %d x %d mesh\n", n, m);
    
    StartTimer();    
    int iter = 0;
    
    while ( /*error > tol &&*/ iter < iter_max )
    {
        error = 0.0;      
        for( j = 1; j < n-1; j++)
        {
            for( i = 1; i < m-1; i++ )
            {          
              
                  Anew[j][i] = 0.25 * ( A[j][i+1] + A[j][i-1]
                              + A[j-1][i] + A[j+1][i]);    
                
  
              error = MAX(error, fabs(Anew[j][i] - A[j][i]));            
            }
        }
   
   for( j = 1; j < n-1; j++)
        {
            for( i = 1; i < m-1; i++ )
            {
                 A[j][i] = Anew[j][i];    
            }
        }
     

        /*if(iter % 100 == 0)*/ printf("%5d, %0.6f\n", iter, error);
        
        iter++;
    }
 

   double runtime = GetTimer();     

    /*writeSolution(error); 
    int passed = checkSolution(error);
    if (passed == 0) {
  printf("Error check passed.\n");
    } else {
  printf("Error check failed.\n");
    }*/
  
  
   //printf(" total: %f s\n", runtime / 1000);   
   return 0;   
}

/*int checkSolution(double error) {
    int j,i;
    double tol = 1.0e-22;
    FILE *solFile;
    solFile = fopen("solution.txt","r");
    double value;
    if (solFile == NULL) {
  printf("Error: Solution could not be checked as the solution file could not be read in.\n");
    } else {
  fscanf(solFile, "%lf", &value);
  if (fabs(value - error) > tol) {
    printf("Error: Results are not correct: Found error: %24.22f, correct error: %24.22f\n",error, value);
    return 1;
  }
        for(j = 0; j < NN; j++) {
            for(i = 0; i < NM; i++)  {
                fscanf(solFile, "%lf", &value);
    if (fabs(value - A[j][i]) > tol) {
       printf("Error: Results are not correct. See [j,i] = [%d,%d]: found: %24.22f, correct: %24.22f\n",j,i,A[j][i],value);
       return 1;
    }
            }
        }
        fclose(solFile);   hiiii
    }
    return 0; 
}
*/
void writeSolution(double error) {
    double A[NN][NM];//djaasssss  normalement fait avec le #define
    int j,i;
    FILE *solFile;
    solFile = fopen("solution.txt","w");
    if (solFile == NULL) {
  printf("Error: Solution could not be written.\n");
    } else {
  fprintf(solFile, "%24.22f\n", error);
        for(j = 0; j < NN; j++) {
            for(i = 0; i < NM; i++)  {
                fprintf(solFile, "%24.22f ", A[j][i]);
            }
      fprintf(solFile, "\n");
        }
  fclose(solFile);
    }
}

