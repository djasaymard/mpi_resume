#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
int main(int argc, char** argv) {
 //ce fichier a servi pour voir differents concepts,...
 //... les parties actuelement commentées marchent tout autant que celles non commentées 

    /*MPI_Init(NULL, NULL);
    int nb_mpi, id;
    MPI_Comm_size(MPI_COMM_WORLD, &nb_mpi);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    printf("bonjour de la part de %d  sur %d \n",id+1, nb_mpi);
    MPI_Finalize();*/

 /* //partie sur une reduction simple   
     int id, pop,val, somme, alt=0;
     MPI_Status status;
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &pop);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    val=id + 1;

    MPI_Reduce(&val,&somme,1, MPI_INT, MPI_SUM,0, MPI_COMM_WORLD);
    
    if(id==0)
    {
    printf("la reduction des valeurs donne %d \n", somme);
    alt=pop*(pop+1)/2;
    printf("autre calcul %d\n", alt);
    }
    MPI_Finalize();*/

    //Partie sur l'envoie des valeurs sur son voisin
    
   /* int positive_modulo(int i, int n)    {
    	return (i%n + n)%n;
    }

    int id,p,val, somme, alt=0, res=0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Status status;
    
     val=id+1;
     if(id!=1)
     {
         MPI_Recv(&res,1,MPI_INT, positive_modulo(id-1, p),0,MPI_COMM_WORLD, &status);
     }
     val=val + res;
     if (id!=0)
     {
     	MPI_Send(&val,1, MPI_INT, positive_modulo(id+1,p),0,MPI_COMM_WORLD);
     }

     if (id==0)
     {
        somme==val;
     	printf("reduction sur process %d donne %d \n:",id, val);
     	alt=p*(p+1)/2;
     	printf("autre valeur %d: \n", alt);
     }
     else
     {
      printf("sur les autres process: \n");
      printf("id process %d valeur  %d \n:",id, val);
     }
    MPI_Finalize();
    return EXIT_SUCCESS;*/

//partie sur distribuer un tableau
   MPI_Init(&argc, &argv);
    int id,p;
    
    MPI_Comm_rank(MPI_COMM_WORLD,&id);
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    int **creertab(int ligne, int colonne)  {
        int **tab=(int**)malloc(sizeof(int*) * ligne);
    for(int i=0; i<ligne; i++)    {
        tab[i]= (int*)malloc(sizeof(int) *colonne);
    }
    return tab;
   }

   int **inittab(int **tab, int ligne, int colonne) {
    for(int i = 0; i < ligne;i++)    {
    	for(int j=0; j<colonne;j++){
    		tab[i][j]=i+1;   	}
    }
    return tab;
   }
    
    void affichertab(int **tab) { 
         for(int i = 0; i < 4; i++)
        {
        	for(int j=0; j<4;j++){
    		printf ("%d",tab[i][j]);
    		}
         	printf ("\n");
        }
    }
    //----------------------
     int ** montab= creertab(4,4);
     int ** montabinit=  inittab(montab, 4,4);
     affichertab(montabinit);

     //transformons le tableau


    MPI_Finalize();
    return EXIT_SUCCESS;

}
